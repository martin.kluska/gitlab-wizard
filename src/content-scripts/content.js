import fillApprovers from './mergeRequests/fillApprovers.js';
import fillReviewers from './mergeRequests/fillReviewers.js';
import fillOptions from './mergeRequests/fillOptions.js';
import fillLabels from './mergeRequests/fillLabels.js';
import fillTemplate from './mergeRequests/fillTemplate.js';
import './mergeRequests/resolvedThreads.js';

global.browser = require('webextension-polyfill');

browser.runtime.onMessage.addListener(async ({ command, payload }) => {
    if (command === 'fillApprovers') {
        return fillApprovers(payload);
    } else if (command === 'fillReviewers') {
        return fillReviewers(payload);
    } else if (command === 'fillOptions') {
        return fillOptions(payload);
    } else if (command === 'fillLabels') {
        return fillLabels(payload);
    } else if (command === 'fillTemplate') {
        return fillTemplate(payload);
    }
});
